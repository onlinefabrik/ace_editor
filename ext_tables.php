<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

if (TYPO3_MODE === 'BE') {
    // Registers a Backend Module
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'CodingMs.ace_editor',
        'file',
        'aceeditor',
        '',
        [
            'AceEditor' => 'list,edit,save',
        ],
        [
            'access' => 'user,group',
            'icon' => 'EXT:ace_editor/Resources/Public/Icons/module-aceeditor.svg',
            'iconIdentifier' => 'module-aceeditor',
            'labels' => 'LLL:EXT:ace_editor/Resources/Private/Language/locallang_aceeditor.xlf',
        ]
    );
}
//
// register svg icons: identifier and filename
$iconsSvg = [
    'module-aceeditor' => 'Resources/Public/Icons/module-aceeditor.svg',
];
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
foreach ($iconsSvg as $identifier => $path) {
    $iconRegistry->registerIcon(
        $identifier,
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:ace_editor/' . $path]
    );
}
