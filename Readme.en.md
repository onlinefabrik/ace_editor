# ACE-Editor for TYPO3

This extension provides an ACE-Editor for TYPO3 backend.

## Editable files

The ACE-editor can only edit file types, which are included in the `[SYS][textfile_ext]`. If you need additional file types, just add the required file extension in install tool.
