# ACE-Editor für TYPO3

Diese Erweiterung integriert den ACE-Editor in das TYPO3-Backend.

## Bearbeitbare Dateien

Der ACE-Editor kann nur Dateitypen bearbeiten, welche auch in der Einstellung `[SYS][textfile_ext]` gelistet sind. Wenn Sie zusätzliche Dateitypen benötigen, können Sie diese einfach über das Installtool hinzufügen.
