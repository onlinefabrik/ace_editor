<?php

namespace CodingMs\AceEditor\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Thomas Deuling <typo3@coding.ms>, coding.ms
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\FileRepository;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use TYPO3\CMS\Backend\View\BackendTemplateView;
use TYPO3\CMS\Backend\Template\Components\ButtonBar;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Backend\Routing\UriBuilder as Url;

/**
 * Ace editor controller
 */
class AceEditorController extends ActionController
{

    /**
     * BackendTemplateContainer
     *
     * @var BackendTemplateView
     */
    protected $view;

    /**
     * Backend Template Container
     *
     * @var BackendTemplateView
     */
    protected $defaultViewObjectName = BackendTemplateView::class;

    /**
     * @var IconFactory
     */
    protected $iconFactory;

    /**
     * @var string
     */
    protected $folder;

    /**
     * Set up the doc header properly here
     *
     * @param ViewInterface $view
     * @throws \Exception
     */
    protected function initializeView(ViewInterface $view)
    {
        $this->folder = GeneralUtility::_GP('id');
        // Initialize icon factory
        $this->iconFactory = GeneralUtility::makeInstance(IconFactory::class);
        if ($this->view->getModuleTemplate() !== null) {
            $pageRenderer = $this->view->getModuleTemplate()->getPageRenderer();
            $pageRenderer->addCssFile( 'EXT:ace_editor/Resources/Public/Stylesheets/AceEditor.css');
            // Require Ace editor
            if($this->actionMethodName == 'editAction') {
                $contribPath = 'Resources/Public/Contrib/';
                $contribPath = ExtensionManagementUtility::extPath('ace_editor', $contribPath);
                $pageRenderer->addRequireJsConfiguration([
                    'shim' => [],
                    'paths' => [
                        'ace' => PathUtility::getAbsoluteWebPath($contribPath) . 'ace/src'
                    ]
                ]);
                $pageRenderer->loadRequireJsModule('TYPO3/CMS/AceEditor/AceEditor');
            }
            // Initialize icon factory
            $this->iconFactory = GeneralUtility::makeInstance(IconFactory::class);
            // Create menu and buttons
            $this->createButtons();
        }
    }

    /**
     * Add menu buttons for specific actions
     *
     * @return void
     * @throws \Exception
     */
    protected function createButtons()
    {
        $fileUid = '';
        if($this->request->hasArgument('file')) {
            $fileUid = $this->request->getArgument('file');
        }
        $moduleName = 'file_AceEditorAceeditor';
        $buttonBar = $this->view->getModuleTemplate()->getDocHeaderComponent()->getButtonBar();
        $uriBuilder = $this->objectManager->get(UriBuilder::class);
        $uriBuilder->setRequest($this->request);
        $buttons = [];
        switch ($this->request->getControllerActionName()) {
            case 'list': {
                // No actions yet
                break;
            }
            case 'edit': {
                // Cancel button
                $parameter = [
                    'id' => $this->folder,
                    'tx_aceeditor_' . strtolower($moduleName) => [
                        'action' => 'list',
                        'controller' => 'AceEditor'
                    ]
                ];
                $uriCancel = $this->uriBuilder->setArguments($parameter)->buildBackendUri();
                $buttons[] = $buttonBar->makeLinkButton()
                    ->setHref($uriCancel)
                    ->setTitle($this->translate('tx_aceeditor_label.edit_file_cancel'))
                    ->setClasses('editor-close')
                    ->setIcon($this->getIcon('actions-close'));
                // Save button
                $buttons[] = $buttonBar->makeLinkButton()
                    ->setHref($uriCancel)
                    ->setTitle($this->translate('tx_aceeditor_label.edit_file_save'))
                    ->setClasses('editor-save')
                    ->setIcon($this->getIcon('actions-document-save'));
                // Save and close button
                $buttons[] = $buttonBar->makeLinkButton()
                    ->setHref($uriCancel)
                    ->setTitle($this->translate('tx_aceeditor_label.edit_file_save_and_close'))
                    ->setClasses('editor-save-and-close')
                    ->setIcon($this->getIcon('actions-document-save-close'));
                break;
            }
        }
        foreach ($buttons as $button) {
            $buttonBar->addButton($button, ButtonBar::BUTTON_POSITION_LEFT);
        }
        //
        // Refresh
        $refresh = [
            'id' => $this->folder,
            'tx_aceeditor_' . strtolower($moduleName) => [
                'action' => $this->request->getControllerActionName(),
                'controller' => 'AceEditor'
            ]
        ];
        if($fileUid !== '') {
            $refresh['tx_aceeditor_' . strtolower($moduleName)]['file'] = $fileUid;
        }
        $uriBuilder = GeneralUtility::makeInstance(Url::class);
        $refreshUrl = (string)$uriBuilder->buildUriFromRoute($moduleName, $refresh);
        #$refreshUrl = BackendUtility::getModuleUrl($moduleName, $refresh);
        $refreshButton = $buttonBar->makeLinkButton()
            ->setHref($refreshUrl)
            ->setTitle($this->translate('tx_aceeditor_label.refresh_view'))
            ->setIcon($this->getIcon('actions-refresh'));
        $buttonBar->addButton($refreshButton, ButtonBar::BUTTON_POSITION_RIGHT);
        //
        // Bookmarks
        $mayMakeShortcut = $this->getBackendUser()->mayMakeShortcut();
        if ($mayMakeShortcut) {
            $extensionName = $this->request->getControllerExtensionName();
            $modulePrefix = strtolower('tx_' . $extensionName . '_' . $moduleName);
            $getVars = ['id', 'M', $modulePrefix];
            $shortcutButton = $buttonBar->makeShortcutButton()->setModuleName($moduleName)->setGetVariables($getVars);
            $buttonBar->addButton($shortcutButton);
        }
    }

    /**
     * Returns the currently logged in BE user
     *
     * @return BackendUserAuthentication
     */
    protected function getBackendUser()
    {
        return $GLOBALS['BE_USER'];
    }

    /**
     * @param $key
     * @return Icon
     * @version 1.0.0
     */
    protected function getIcon($key) {
        return $this->iconFactory->getIcon($key, Icon::SIZE_SMALL);
    }

    /**
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     */
    public function listAction() {
        /** @var ResourceFactory $resourceFactory */
        $resourceFactory = $this->objectManager->get(ResourceFactory::class);
        $folder = $resourceFactory->getFolderObjectFromCombinedIdentifier($this->folder);
        $this->view->assign('folder', $folder);
        $recursive = false;
        if($this->request->hasArgument('recursive')) {
            $recursive = $this->request->getArgument('recursive');
        }
        $types = [
            'items' => GeneralUtility::trimExplode(',', $GLOBALS['TYPO3_CONF_VARS']['SYS']['textfile_ext'], true),
            'selected' => '',
        ];
        $types['items'] = array_combine($types['items'], $types['items']);
        if($this->request->hasArgument('type')) {
            $types['selected'] = trim($this->request->getArgument('type'));
        }
        $files = $folder->getFiles(0, 0, 0, $recursive);
        $filesFiltered = [];
        /** @var File $file */
        foreach($files as $file) {
            $type = $file->getExtension();
            if($types['selected'] === $type) {
                $filesFiltered[] = $file;
            }
        }
        if($types['selected'] !== '') {
            $files = $filesFiltered;
        }
        $this->view->assign('types', $types);
        $this->view->assign('recursive', $recursive);
        $this->view->assign('files', $files);
    }

    /**
     * Edit file content
     *
     * @return void
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     */
    public function editAction()
    {
        $file = null;
        $content = null;
        if($this->request->hasArgument('file')) {
            $fileUid = $this->request->getArgument('file');
            /** @var FileRepository $fileRepository */
            $fileRepository = $this->objectManager->get(FileRepository::class);
            /** @var File $file */
            $file = $fileRepository->findByIdentifier($fileUid);
            $content = $file->getContents();
        }
        $this->view->assign('wrapper', 'ace');
        $this->view->assign('file', $file);
        $this->view->assign('content', $content);
    }

    /**
     * Save file content by using AJAX
     *
     * @return void
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     */
    public function saveAction()
    {
        $json = [];
        if($this->request->hasArgument('content')) {
            $content = $this->request->getArgument('content');
            if($this->request->hasArgument('file')) {
                $fileUid = $this->request->getArgument('file');
                /** @var FileRepository $fileRepository */
                $fileRepository = $this->objectManager->get(FileRepository::class);
                /** @var File $file */
                $file = $fileRepository->findByIdentifier($fileUid);
                if($file instanceof File) {
                    $file->setContents($content);
                    $json['success'] = 'File saved';
                }
                else {
                    $json['error'] = 'File not found';
                }
            }
            else {
                $json['error'] = 'No file passed';
            }
        }
        else {
            $json['error'] = 'No content passed';
        }
        header('Content-Type: application/json; charset=UTF-8');
        echo json_encode($json);
        exit;
    }

    /**
     * @param $key
     * @param array $arguments
     * @return NULL|string
     * @version 1.0.0
     * @throws \Exception
     */
    protected function translate($key, $arguments=[]) {
        $translation = LocalizationUtility::translate($key, 'ace_editor', $arguments);
        if($translation === null) {
            $extensionKey = GeneralUtility::camelCaseToLowerCaseUnderscored($this->extensionName);
            $path = 'EXT:' . $extensionKey . '/Resources/Private/Language/locallang.xlf';
            throw new \Exception('Translation ' . $key . ' not found in ' . $path);
        }
        return LocalizationUtility::translate($key, 'ace_editor', $arguments);
    }

}
